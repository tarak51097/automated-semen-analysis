clc
clear all
close all
addpath('D:\60013150001');
rgb=imread('sperm.jpg');
subplot(3,3,1)
imshow(rgb); title('Original image');
I=rgb2gray(rgb);
subplot(3,3,2)
imshow(I);title('RGB to GRAY image');
% text(732,501,'semen analysis',...
%      'FontSize',7,'HorizontalAlignment','right');

% Step 2: Use the Gradient Magnitude as the Segmentation Function
hy = fspecial('sobel');
hx = hy';
Iy = imfilter(double(I), hy, 'replicate');
Ix = imfilter(double(I), hx, 'replicate');
gradmag = sqrt(Ix.^2 + Iy.^2);
subplot(3,3,3), imshow(gradmag,[]), title('Gradient magnitude (gradmag)');

% Step 3: Mark the Foreground Objects
se = strel('disk', 20);
Io = imopen(I, se);
subplot(3,3,4), imshow(Io), title('Opening (Io)')

Ie = imerode(I, se);
Iobr = imreconstruct(Ie, I);
subplot(3,3,5),imshow(Iobr), title('Opening-by-reconstruction (Iobr)');

Ioc = imclose(Io, se);
subplot(3,3,6), imshow(Ioc), title('Opening-closing (Ioc)');

Iobrd = imdilate(Iobr, se);
Iobrcbr = imreconstruct(imcomplement(Iobrd), imcomplement(Iobr));
Iobrcbr = imcomplement(Iobrcbr);
subplot(3,3,7) , imshow(Iobrcbr), title('Opening-closing by reconstruction (Iobrcbr)');

fgm = imregionalmax(Iobrcbr);
subplot(3,3,8), imshow(fgm), title('Regional maxima of opening-closing by reconstruction (fgm)');

I2 = I;
I2(fgm) = 255;
subplot(3,3,9), imshow(I2), title('Regional maxima superimposed on original image (I2)');

fgm4 = bwareaopen(fgm, 20);
I3 = I;
I3(fgm4) = 255;
figure(2),subplot(3,3,1), imshow(I3)
title('Modified regional maxima superimposed on original image (fgm4)');

% Step 4: Compute Background Markers
bw = im2bw(Iobrcbr, graythresh(Iobrcbr));
figure(2),subplot(3,3,2), imshow(bw), title('Thresholded opening-closing by reconstruction (bw)');

D= bwdist(bw);
DL = watershed(D);
bgm = DL == 0;
figure(2),subplot(3,3,3), imshow(bgm), title('Watershed ridge lines (bgm)');

% Step 5: Compute the Watershed Transform of the Segmentation Function.
    gradmag2 = imimposemin(gradmag, bgm | fgm4);
    L = watershed(gradmag2);
    I4 = I;
I4(imdilate(L == 0, ones(3, 3)) | bgm | fgm4) = 255;
figure(2),subplot(3,3,4), imshow(I4);
title('Markers and object boundaries superimposed on original image (I4)');


Lrgb = label2rgb(L, 'jet', 'w', 'shuffle');
figure(2),subplot(3,3,5), imshow(Lrgb)
title('Colored watershed label matrix (Lrgb)')

figure(2),subplot(3,3,6), imshow(I), hold on
himage = imshow(Lrgb);
set(himage, 'AlphaData', 0.3);
title('Lrgb superimposed transparently on original image');
    
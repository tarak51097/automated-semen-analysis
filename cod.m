%semen analysis image using Correcting Nonuniform Illumination from Matlab
%   help,google and review paper of IEEE

%https://in.mathworks.com/help/images/examples/correcting-nonuniform-illumination.html

clc
clear all
close all
clearvars;
% workspace;
g = imread('sperm.jpg');
subplot(4,4,1),imshow(g),title(' original image' );

I=rgb2gray(g);
subplot(4,4,2),imshow(I);
% background = imopen(I,strel('disk',15));

% Display the Background Approximation as a Surface
% figure
% subplot(4,4,2),
% surf(double(background(1:8:end,1:8:end))),zlim([0 255]);
% ax = gca;
% ax.YDir = 'reverse';
% 
% I2 = I - background;

%A single step using imtophat which first calculates the morphological opening and then subtracts it from the original image.
I2 = imtophat(I,strel('disk',350));
subplot(4,4,3),imagesc(I2);

%step:4 Increase the Image Contrast
I3 = imadjust(I2);
subplot(4,4,4),imagesc(I3);

%step 5 : threshold the image
% bw = im2b(I3);
bw = bwareaopen(I3, 50);
subplot(4,4,5),imshow(bw);

%idebtify objects in the image
cc = bwconncomp(bw,4);


no_of_sperms =cc.NumObjects-1

% %examine one object
% sperm = false(size(bw));
% sperm(cc.PixelIdxList{1}) = true;
% subplot(4,4,6),imshow(sperm);
% figure(2), imhist(sperm);

%view all objects
labeled = labelmatrix(cc);
whos labeled

RGB_label = label2rgb(labeled, @spring, 'c', 'shuffle');
subplot(4,4,7),imshow(RGB_label);
% 
%Step 9: Compute Area of Each Object
spermdata = regionprops(cc,'basic')
% 
% spermdata(3).Area
% 
%Step 10: Compute Area-based Statistics
sperm_areas = [spermdata.Area];
% 
%Find the sperm with the smallest area.
   [min_area, idx] = min(sperm_areas)
   
   sperm = false(size(bw));
sperm(cc.PixelIdxList{idx}) = true;
subplot(4,4,7),imshow(sperm);

% %create histogram
% figure(2), imhist(sperm_areas),
% title('Histogram of sperm object Area');


workspace;





